package burp;

import burp.j2ee.CustomScanIssue;
import burp.j2ee.Risk;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTTPMatcher {

    
    /** 
     * Parse the web.xml J2EE resource, and pretty print servlet classes 
     * into the burp report
     * 
     * @param webxml content of the web.xml configuration file
     * @return return a user friendly list with HTML formatted code, 
     * of all servlet classes defined into the web.xml file
     */
    public static String getServletsDescription(String webxml){
        List<String> servlets = getServletsFromWebDescriptors(webxml);
        String description = "";
        if (servlets.isEmpty()){
            return description;
        }
        
        description += "<br /><br />List of remote Java classes used by the application:<br /><ul>";
        
        for (String servlet : servlets){
            description += "<li><b>" + servlet + "</b></li>";
        }
        description +="</ul><br /><br />It's possible to download the above classes "
                + "located in <i>WEB-INF/classes/</i> folder";
        return description;
    }
    
    /**
     * Parse the servlet classes from a web.xml file
     * 
     * @param webxml content of the web.xml configuration file
     * @return list of servlet classes defined into the web.xml file
     */
    public static List<String> getServletsFromWebDescriptors(String webxml){
        List<String> servlets = new ArrayList();
        
        Pattern servletMatcher = Pattern.compile("<servlet-class>(.*?)</servlet-class>", Pattern.DOTALL | Pattern.MULTILINE);
        
        Matcher matcher = servletMatcher.matcher(webxml);
        while (matcher.find()) {
            int numEntries = matcher.groupCount();
            for (int i=1;  i <= numEntries; i++){
                servlets.add(matcher.group(i).trim().replace("\n", "").replace("\r", ""));
            }            
        }
        
        return servlets;        
    }
    
    
    /**
     * From the Apache Axis Service Page, parse and retrieve the available web services installed
     * on the remote system
     * 
     * @param axisServiceListPage the content of Apache Axis Services page
     * @return a list with the names of all Apache Axis Services
     */
    public static List<String> getServicesFromAxis(String axisServiceListPage){
        List<String> wsName = new ArrayList();
        
        Pattern servletMatcher = Pattern.compile("services/(.*?)\\?wsdl",  Pattern.MULTILINE);
        
        Matcher matcher = servletMatcher.matcher(axisServiceListPage);
        while (matcher.find()) {
            int numEntries = matcher.groupCount();
            for (int i=1;  i <= numEntries; i++){
                wsName.add(matcher.group(i).trim().replace("\n", "").replace("\r", ""));
            }            
        }
        
        return wsName;      
    }
    
    public static Boolean isXML(String value) {
        if (value == null) {
            return false;
        }
        return value.trim().startsWith("<");
    }
    

    /**
     * Helper method to search a response for occurrences of a literal match
     * string and return a list of start/end offsets
     */
    public static List<int[]> getMatches(byte[] response, byte[] match, IExtensionHelpers helpers) {
        List<int[]> matches = new ArrayList<int[]>();

        int start = 0;
        while (start < response.length) {
            start = helpers.indexOf(response, match, true, start, response.length);
            if (start == -1) {
                break;
            }
            matches.add(new int[]{start, start + match.length});
            start += match.length;
        }

        return matches;
    }

    
    public static boolean isEtcPasswdFile(byte[] response, IExtensionHelpers helpers){
        final byte[] PASSWD_PATTERN = "root:".getBytes();
        List<int[]> matchesPasswd = getMatches(response, PASSWD_PATTERN, helpers);
        
        return (matchesPasswd.size() > 0);        
    }

    public static boolean isEtcShadowFile(byte[] response, IExtensionHelpers helpers){
        final byte[] SHADOW_PATTERN = "root:".getBytes();
        List<int[]> matchesShadow = getMatches(response, SHADOW_PATTERN, helpers);
        
        return (matchesShadow.size() > 0);        
    }

    // http://www-01.ibm.com/support/knowledgecenter/SSEQTP_7.0.0/com.ibm.websphere.base.doc/info/aes/ae/tweb_jspengine.html       
    public static boolean isIBMWebExtFile(byte[] response, IExtensionHelpers helpers){
        final byte[] IBMWEB_PATTERN = "<jspAttributes".getBytes();
        List<int[]> matchesIbmweb = getMatches(response, IBMWEB_PATTERN, helpers);
        
        return (matchesIbmweb.size() > 0);        
    }
            
    public static boolean isApacheStrutsConfigFile(byte[] response, IExtensionHelpers helpers){
        final byte[] STRUTS_PATTERN = "<struts".getBytes();
        List<int[]> matchesStruts = getMatches(response, STRUTS_PATTERN, helpers);
        
        return (matchesStruts.size() > 0);        
    }    

    public static boolean isSpringContextConfigFile(byte[] response, IExtensionHelpers helpers){
        final byte[] SPRING_PATTERN = "<beans".getBytes();
        List<int[]> matchesStruts = getMatches(response, SPRING_PATTERN, helpers);
        
        return (matchesStruts.size() > 0);        
    }    
    
    public static boolean isWebLogicFile(byte[] response, IExtensionHelpers helpers){
        final byte[] WEBLOGIC_PATTERN = "<weblogic-web-app>".getBytes();
        List<int[]> matchesWebLogic = getMatches(response, WEBLOGIC_PATTERN, helpers);
        
        return (matchesWebLogic.size() > 0);        
    }  
    
    /**
     * Detect the application context of the given URL
     *
     * Ex: http://www.example.org/myapp/test.jsf
     *
     * returns myapp
     */
    public static String getApplicationContext(URL url) {

        String host = url.getHost();
        String protocol = url.getProtocol();
        String path = url.getPath();
        int port = url.getPort();

        int i = path.indexOf("/", 1);
        String context = path.substring(0, i + 1);

        return context;
    }

    public static List<IScanIssue> getVulnerabilityByPageParsing(IHttpRequestResponse baseRequestResponse,
            IBurpExtenderCallbacks callbacks) {

        List<IScanIssue> issues = new ArrayList<>();
        IExtensionHelpers helpers = callbacks.getHelpers();
        IRequestInfo reqInfo = helpers.analyzeRequest(baseRequestResponse);

        byte[] rawRequest = baseRequestResponse.getRequest();
        byte[] rawResponse = baseRequestResponse.getResponse();

        String req = helpers.bytesToString(rawRequest);
        String body = req.substring(reqInfo.getBodyOffset());

        byte[] jsfException = "<pre><code>com.sun.facelets.FaceletException".getBytes();
        List<int[]> matchesJsf = getMatches(rawResponse, jsfException, helpers);
        if (matchesJsf.size() > 0) {

            issues.add(new CustomScanIssue(
                    baseRequestResponse.getHttpService(),
                    helpers.analyzeRequest(baseRequestResponse).getUrl(),
                    baseRequestResponse,
                    "Incorrect Error Handling - JSF",
                    "The remote application does not properly handle application errors, "
                    + "and application stacktraces are dispalyed to the end user "
                    + "leading to information disclosure vulnerability",
                    Risk.LOW));

        }

        byte[] strutsDevMode = "<title>Struts Problem Report</title>".getBytes();
        List<int[]> matchesStrutsDev = getMatches(rawResponse, strutsDevMode, helpers);
        if (matchesStrutsDev.size() > 0) {

            issues.add(new CustomScanIssue(
                    baseRequestResponse.getHttpService(),
                    helpers.analyzeRequest(baseRequestResponse).getUrl(),
                    baseRequestResponse,
                    "Apache Struts - DevMode Enabled",
                    "The remote application  is configured for"
                    + " a development enviroment; development mode, or devMode, enables extra\n"
                    + "debugging behaviors and reports to assist developers.",
                    "Disable development mode in production enviroments using "
                    + "the property <i>struts.devMode=false</i>",
                    Risk.LOW));
        }

        List<byte[]> javaxServletExceptions = Arrays.asList(
                "javax.servlet.ServletException".getBytes(),
                "onclick=\"toggle('full exception chain stacktrace".getBytes(),
                "at org.apache.catalina".getBytes(),
                "at org.apache.coyote.".getBytes(),
                "at org.jboss.seam.".getBytes(),
                "at org.apache.tomcat.".getBytes());

        for (byte[] exc : javaxServletExceptions) {

            List<int[]> matchesJavax = getMatches(rawResponse, exc, helpers);
            if (matchesJavax.size() > 0) {

                issues.add(new CustomScanIssue(
                        baseRequestResponse.getHttpService(),
                        helpers.analyzeRequest(baseRequestResponse).getUrl(),
                        baseRequestResponse,
                        "Incorrect Error Handling - Java",
                        "The remote application does not properly handle application errors, "
                        + "and application stacktraces are dispalyed to the end user "
                        + "leading to information disclosure vulnerability",
                        Risk.LOW));
            }

        }

        
        /**
         * SQL statements in URL 
         * 
         * Improved detection for SQL statements in HTTP POST requests.
         */
        if (body != null) {
                       
            List<Pattern> sqlQueriesRe = new ArrayList();
            sqlQueriesRe.add(Pattern.compile("select ", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE));
            sqlQueriesRe.add(Pattern.compile("IS NOT NULL", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE));

            // check the pattern on response body
            for (Pattern sqlQueryRule : sqlQueriesRe) {

                Matcher matcher = sqlQueryRule.matcher(helpers.urlDecode(body));

                if (matcher.find()) {
                    issues.add(new CustomScanIssue(
                            baseRequestResponse.getHttpService(),
                            helpers.analyzeRequest(baseRequestResponse).getUrl(),
                            baseRequestResponse,
                            "SQL Statements in HTTP Request",
                            "The remote application seems to use SQL Statements in"
                            + "HTTP request",
                            Risk.MEDIUM));
                }
            }

        }

        return issues;
    }

}
