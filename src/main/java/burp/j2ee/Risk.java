
package burp.j2ee;


public class Risk {
    public static final String HIGH  = "High";
    public static final String MEDIUM  = "Medium";
    public static final String LOW  = "Low";
    public static final String INFORMATION  = "Information";
}
